# Configuration for JavaScript

This project uses [Webpack](https://webpack.js.org/) to bundle JavaScript files. Webpack is a module bundler that takes modules with dependencies and generates static assets representing those modules.

## Installation

### vuetify

```bash
npm create

cd simple-project-manager
npm install
npm install vuetify  # add vuetify to the project

npm run format
npm run dev

```
