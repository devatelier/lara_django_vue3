# Data Exchange via gRPC


mport base64
import binascii
import imghdr
import io
import uuid
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.translation import gettext as _
from rest_framework.fields import CharField, DateTimeField, Field, FileField, ImageField
class Base64FieldMixin(object):
    EMPTY_VALUES = (None, "", [], (), {})
    @property
    def ALLOWED_TYPES(self):
        raise NotImplementedError
    @property
    def INVALID_FILE_MESSAGE(self):
        raise NotImplementedError
    @property
    def INVALID_TYPE_MESSAGE(self):
        raise NotImplementedError
    def __init__(self, *args, **kwargs):
        self.trust_provided_content_type = kwargs.pop("trust_provided_content_type", False)
        self.represent_in_base64 = kwargs.pop("represent_in_base64", False)
        super(Base64FieldMixin, self).__init__(*args, **kwargs)
    def to_internal_value(self, base64_data):
        if base64_data in self.EMPTY_VALUES:
            return None
        if base64_data.startswith(settings.MEDIA_URL):
            # HACK - AM - 05/02/2022 - the data return the media url but we need to remove it befor save it
            base64_data = base64_data[len(settings.MEDIA_URL) :]
            return base64_data
        storage_base_string = f"{settings.FILE_STORAGE_URL}/{settings.GS_BUCKET_NAME}"
        if getattr(
            settings, "DEFAULT_FILE_STORAGE", None
        ) == "storages.backends.gcloud.GoogleCloudStorage" and base64_data.startswith(
            storage_base_string
        ):
            index_of_query_separation = base64_data.index("?")
            # HACK - AM - 05/02/2022 - the data return the media url but we need to remove it befor save it
            base64_data = base64_data[len(storage_base_string) : index_of_query_separation]
            return base64_data
        # Check if this is a base64 string
        if isinstance(base64_data, str):
            file_mime_type = None
            # Strip base64 header, get mime_type from base64 header.
            if ";base64," in base64_data:
                header, base64_data = base64_data.split(";base64,")
                if self.trust_provided_content_type:
                    file_mime_type = header.replace("data:", "")
            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(base64_data)
            except (TypeError, binascii.Error, ValueError):
                raise ValidationError(self.INVALID_FILE_MESSAGE)
            # Generate file name:
            file_name = self.get_file_name(decoded_file)
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)
            if file_extension not in self.ALLOWED_TYPES:
                raise ValidationError(self.INVALID_TYPE_MESSAGE)
            complete_file_name = file_name + "." + file_extension
            data = SimpleUploadedFile(
                name=complete_file_name, content=decoded_file, content_type=file_mime_type
            )
            return super(Base64FieldMixin, self).to_internal_value(data)
        raise ValidationError(
            _("Invalid type. This is not an base64 string: {}".format(type(base64_data)))
        )
    def get_file_extension(self, filename, decoded_file):
        raise NotImplementedError
    def get_file_name(self, decoded_file):
        return str(uuid.uuid4())
    def to_representation(self, file):
        if self.represent_in_base64:
            # If the underlying ImageField is blank, a ValueError would be
            # raised on `open`. When representing as base64, simply return an
            # empty base64 str rather than let the exception propagate unhandled
            # up into serializers.
            if not file:
                return ""
            try:
                with open(file.path, "rb") as f:
                    return base64.b64encode(f.read()).decode()
            except Exception:
                raise IOError("Error encoding file")
        else:
            return super(Base64FieldMixin, self).to_representation(file)
class Base64FileField(Base64FieldMixin, FileField):
    """
    A django-rest-framework field for handling file-uploads through raw post data.
    It uses base64 for en-/decoding the contents of the file.
    """
    @property
    def ALLOWED_TYPES(self):
        raise NotImplementedError("List allowed file extensions")
    INVALID_FILE_MESSAGE = _("Please upload a valid file.")
    INVALID_TYPE_MESSAGE = _("The type of the file couldn't be determined.")
    def get_file_extension(self, filename, decoded_file):
        raise NotImplementedError("Implement file validation and return matching extension.")
class PDFBase64File(Base64FileField):
    ALLOWED_TYPES = ["pdf"]
    def get_file_extension(self, filename, decoded_file):
        # try:
        #     PyPDF2.PdfFileReader(io.BytesIO(decoded_file))
        # except PyPDF2.utils.PdfReadError as e:
        #     logger.warning(e)
        # else:
        return "pdf"
class ExcelFile(Base64FileField):
    ALLOWED_TYPES = ["xls"]
    def get_file_extension(self, filename, decoded_file):
        # try:
        #     PyPDF2.PdfFileReader(io.BytesIO(decoded_file))
        # except PyPDF2.utils.PdfReadError as e:
        #     logger.warning(e)
        # else:
        return "xls"
class Base64ImageField(Base64FieldMixin, ImageField):
    """
    A django-rest-framework field for handling image-uploads through raw post data.
    It uses base64 for en-/decoding the contents of the file.
    """
    ALLOWED_TYPES = ("jpeg", "jpg", "png", "gif")
    INVALID_FILE_MESSAGE = _("Please upload a valid image.")
    INVALID_TYPE_MESSAGE = _("The type of the image couldn't be determined.")
    def get_file_extension(self, filename, decoded_file):
        try:
            from PIL import Image
        except ImportError:
            raise ImportError("Pillow is not installed.")
        extension = imghdr.what(filename, decoded_file)
        # Try with PIL as fallback if format not detected due
        # to bug in imghdr https://bugs.python.org/issue16512
        if extension is None:
            try:
                image = Image.open(io.BytesIO(decoded_file))
            except (OSError, IOError):
                raise ValidationError(self.INVALID_FILE_MESSAGE)
            extension = image.format.lower()
        extension = "jpg" if extension == "jpeg" else extension
        return extension
class Base64ImageOrPDFField(Base64FileField):
    """
    A django-rest-framework field for handling image-uploads through raw post data.
    It uses base64 for en-/decoding the contents of the file.
    """
    ALLOWED_TYPES = ("jpeg", "jpg", "png", "gif", "pdf")
    INVALID_FILE_MESSAGE = _("Please upload a valid pdf or image.")
    INVALID_TYPE_MESSAGE = _("The type of the image couldn't be determined.")
    def get_file_extension(self, filename, decoded_file):
        try:
            from PIL import Image
        except ImportError:
            raise ImportError("Pillow is not installed.")
        extension = imghdr.what(filename, decoded_file)
        # Try with PIL as fallback if format not detected due
        # to bug in imghdr https://bugs.python.org/issue16512
        if extension is None:
            try:
                image = Image.open(io.BytesIO(decoded_file))
            except (OSError, IOError):
                return "pdf"
            extension = image.format.lower()
        extension = "jpg" if extension == "jpeg" else extension
        return extension
class NullableDatetimeField(DateTimeField):
    def to_internal_value(self, value):
        if not value:
            return None
        return super().to_internal_value(value)
class empty:
    """
    This class is used to represent no data being provided for a given input
    or output value.
    It is required because `None` may be a valid input or output value.
    """
class DefaultIfEmptyCharField(CharField):
    def to_internal_value(self, value):
        if not value:
            return self.get_default()
        return super().to_internal_value(value)
    def run_validation(self, data=None):
        data = Field.run_validation(self, data)
        if data is None and self.default is not empty:
            data = self.get_default()
        return data
class NullableCharField(CharField):
    def to_internal_value(self, value):
        if not value:
            return None
        return super().to_internal_value(value)


----


from django_socio_grpc import proto_serializers
from dresskare_core.serializer_utils.extra_fields import Base64ImageField
from rest_framework import serializers
from rest_framework.fields import UUIDField
import product.grpc.product_pb2 as product_pb2
from product.models import Deposit, DepositImage
class DepositImageProtoSerializer(proto_serializers.ModelProtoSerializer):
    deposit = serializers.PrimaryKeyRelatedField(
        queryset=Deposit.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        allow_null=True,
        required=False,
    )
    image = Base64ImageField(allow_null=True)
    class Meta:
        model = DepositImage
        proto_class = product_pb2.DepositImageResponse
        proto_class_list = product_pb2.DepositImageListResponse
        fields = [
            "uuid",
            "deposit",
            "image",
        ]

---

from django.db import models
from django.utils.translation import gettext as _
from dresskare_core.model_utils.base_model import BaseModel
class DepositImage(BaseModel):
    deposit = models.ForeignKey(
        "product.deposit",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name=_("Deposit"),
        related_name="images",
    )
    image = models.ImageField(
        upload_to="deposits_images", null=True, blank=True, verbose_name=_("Deposit Images")
    )


---- javascript client

async inputFile(newFile) {
      if (!newFile) {
        return;
      }
      this.newFileName = newFile.name;
      const new_value = await this.toBase64(newFile.file);
      // if(this.multiple) {
      //   this.$emit("input", [...this.value, new_value]);
      // } else {
      //   this.$emit("input", new_value);
      // }
      this.$emit("input", new_value);
    },
    toBase64(file) {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = (error) => reject(error);
      });
    },

message DepositImageRequest {
    string uuid = 1;
    optional string deposit = 2;
    optional string image = 3;
}