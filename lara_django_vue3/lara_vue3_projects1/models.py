import datetime
import uuid

from django.db import models
from django.utils import timezone

# from mptt.models import MPTTModel, TreeForeignKey

# Create your models here.


class Project(models.Model):
    project_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.TextField(
        blank=True,
        help_text="human readable project/experiment title - to be displayed in the web frontend",
    )

    # name is obsolete, only name_full is relevant
    name = models.TextField(
        blank=True,
        help_text="name of the project or experiment, should not contain whitespaces",
    )
    # autogenerate this:
    name_full = models.TextField(
        unique=True,
        blank=True,
        help_text="unique, fully qualified project/experiment name, should not contain whitespaces, used for human project descriptions, e.g. de.unigreifswald.biochem.akb.lara.validation",
    )

    acl = models.JSONField(
        blank=True,
        null=True,
        help_text="access control list - fine grained data access control",
    )
    # -> ltree,  will be modelled with the PostgreSQL ltree https://pypi.org/project/django-ltree/
    # ~ parent = models.ForeignKey('self', null=True, blank=True, related_name="parent_projects" ,on_delete=models.CASCADE,
    # ~ help_text="parent projects" )
    # parent = TreeForeignKey(
    #     "self",
    #     null=True,
    #     blank=True,
    #     related_name="%(app_label)s_%(class)s_experiment_parent_related",
    #     related_query_name="%(app_label)s_%(class)s_parent_related_query",
    #     on_delete=models.CASCADE,
    #     help_text="mptt - parent projects",
    # )

    hash_sha256 = models.CharField(
        max_length=256,
        blank=True,
        help_text="SHA256 experiment hash",
    )

    datetime_start = models.DateTimeField(
        null=True,
        blank=True,
        help_text="start of project / experiment",  # datetime(1968, 12, 31, tzinfo=timezone.utc),
    )
    datetime_added = models.DateTimeField(
        null=True,
        blank=True,  # datetime(1968, 12, 31, tzinfo=timezone.utc),
        help_text="datetime project / experiment added to LARA",
    )
    datetime_end = models.DateTimeField(
        null=True,
        blank=True,
        help_text="end of project / experiment",
    )
    datetime_last_modified = models.DateTimeField(
        null=True,
        blank=True,
        help_text="date and time when project / experiment was last modified",
    )

    description = models.TextField(
        blank=True,
        help_text="project / experiment description",
    )
    hypothesis_json = models.JSONField(
        blank=True,
        null=True,
        help_text="hypothesis in machine understandable JSON-LD format.",
    )
    hypothesis = models.TextField(
        blank=True,
        help_text="human readable hypothesis of the experiment",
    )

    results_json = models.JSONField(
        blank=True,
        null=True,
        help_text="results in machine understandable JSON-LD format.",
    )

    remarks = models.TextField(
        blank=True,
        help_text="remarks to the project / experiment",
    )
    iri = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    pid = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="Persitent Identifier [PID/handle URI](https://www.handle.net/)",
    )

    def __str__(self):
        return self.title or ""

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for name_full
        """
        if self.name is None or self.name == "":
            self.name = self.title.strip().replace(" ", "_").lower()

        if self.name_full is None or self.name_full == "":
            self.name_full = self.name.replace(" ", "_").strip().lower()

        if self.datetime_start is None:
            self.datetime_start = timezone.now()

        if self.datetime_end is None:
            self.datetime_end = datetime.datetime(2060, 12, 31, tzinfo=datetime.UTC)

        if self.datetime_added is None:
            self.datetime_added = timezone.now()

        if self.datetime_last_modified is None:
            self.datetime_last_modified = timezone.now()

        super().save(*args, **kwargs)

    def __repr__(self):
        return self.title or ""
