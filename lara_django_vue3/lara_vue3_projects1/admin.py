# Register your models here.
from django.contrib import admin

from .models import Project


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "project_id",
        "title",
        "name",
        "name_full",
        "acl",
        "hash_sha256",
        "datetime_start",
        "datetime_added",
        "datetime_end",
        "datetime_last_modified",
        "description",
        "hypothesis_json",
        "hypothesis",
        "results_json",
        "remarks",
        "iri",
        "pid",
    )
    list_filter = (
        "datetime_start",
        "datetime_added",
        "datetime_end",
        "datetime_last_modified",
    )
    search_fields = ("name",)
