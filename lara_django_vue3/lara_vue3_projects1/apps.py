from django.apps import AppConfig


class LaraVue3Projects1Config(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "lara_django_vue3.lara_vue3_projects1"
